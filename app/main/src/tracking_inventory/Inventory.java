package tracking_inventory;

import java.util.ArrayList;
import java.util.List;

public class Inventory {

    public String invName;

    public Inventory(String invName) {
        this.invName = invName;
    }

    public String headerName = "Name";
    public String headerSerial = "Serial Number";
    public String headerValue = "Value";

    public final List<Product> products = new ArrayList<>();


    public void addNewProduct(String name, String serial, double value){
        Product product = new Product(name, serial, value);
        products.add(product);
    }



    public String listHeader(){
        return String.format("%s | %s | %s\n", headerName, headerSerial, headerValue);
    }

    public String showAsList(){
        String i = listHeader();
        for (Product product:products){
            i+=(String.format("%s | %s | $%s\n", product.name, product.serial, (addSecondDecimalZero(String.valueOf(product.value)).replace('.', ',')) ));
        }
        return i;
    }

    public String CSVHeader(){
        return String.format("%s, %s, %s\n", headerName, headerSerial, headerValue);
    }

    public String toCSV(){
        String i = CSVHeader();
        for (Product product: products){
            i+=(String.format("%s, %s, $%.2f\n", product.name, product.serial, product.value));
        }
        return i;
    }

    public String htmlOpening(String head, String body){
        String html = "<html>";
        return html + head + body +"\n</html>";
    }

    public String htmlHead(String title){
        String head = "\n<head>";
        return head + title + "\n</head>";
    }

    public String htmlTitle(){
        return "\n\t<title>" + Inventory.class.getSimpleName() + "</title>";
    }

    public String htmlBody(String table){
        return String.format(("\n<body>\n\t<h1>%s</h1>%s\n</body>"), Product.class.getSimpleName(), table);
    }

    public String htmlTable(String thread){
        return "\n\t<table>" + thread + "\n\t</table>";
    }

    public String formatValue(String value){
        return "$" + addSecondDecimalZero(value);
    }

    public static String addSecondDecimalZero(String value) {
        if (value.charAt(value.length() - 2) == '.'){
            value += "0";
        }
        return value;
    }

    public String htmlThreadHead(String name, String serial, String value){
        return String.format(("\n\t\t<thread>\n\t\t<tr>") + (("\n")+"\t".repeat(3) + "<th>%s</th>").repeat(3)
                + "\n\t\t</tr>\n\t\t</thread>", name, serial, value);
    }


    public String htmlThread(){
        String result = htmlThreadHead(headerName, headerSerial, headerValue);
        for (Product product: products){
//            result += htmlThreadHead(product.name, product.serial, formatValue(String.valueOf(product.value).replace('.', ',')));
            result += htmlThreadHead(product.name, product.serial, formatValue(String.valueOf(product.value)));
        }
        return result;
    }

    public String searchItem(String search){
        for (Product product: products){
            if (product.name.matches(search) || product.serial.matches(search)){
                return String.format("Found product %s, %s, $%s", product.name, product.serial, addSecondDecimalZero(String.valueOf(product.value)));
            }
        }
        return "No match found.";
    }
    public String searchItem(double search){
        for (Product product: products){
            if (String.valueOf(product.value).matches(String.valueOf(search))){
                return String.format("Found product %s, %s, $%s", product.name, product.serial, addSecondDecimalZero(String.valueOf(product.value)) );
            }
        }
        return "No match found.";
    }




}
