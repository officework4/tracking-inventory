package tracking_inventory;

public class Product {
    protected String name;
    protected String serial;
    protected double value;

    public Product(String name, String serial, double value) {
        this.name = name;
        this.serial = serial;
        this.value = value;
    }

}
