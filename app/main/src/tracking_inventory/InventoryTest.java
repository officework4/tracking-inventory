package tracking_inventory;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InventoryTest {

    Inventory inventory;

    @Before
    public void setUp() {
        inventory = new Inventory("Inventory");
    }

    @Test
    public void add_one_New_Product() {
        inventory.addNewProduct("Xbox One", "AXB124AXY", 399);

        assertEquals(inventory.showAsList(), ("Name | Serial Number | Value\nXbox One | AXB124AXY | $399,00\n"));
    }

    @Test
    public void add_two_new_Products() {
        inventory.addNewProduct("Xbox One", "AXB124AXY", 399);
        inventory.addNewProduct("Samsung TV", "S40AZBDE4", 599);

        assertEquals(inventory.showAsList(), ("Name | Serial Number | Value\n"
                + "Xbox One | AXB124AXY | $399,00\n"
                + "Samsung TV | S40AZBDE4 | $599,00\n"));
    }

    @Test
    public void CSV_print() {
        inventory.addNewProduct("Xbox One", "AXB124AXY", 399);
        inventory.addNewProduct("Samsung TV", "S40AZBDE4", 599);

        assertEquals(inventory.toCSV(), ("Name, Serial Number, Value\n"
                + "Xbox One, AXB124AXY, $399.00\n"
                + "Samsung TV, S40AZBDE4, $599.00\n"));
    }



    @Test
    public void printToHTML() {
        inventory.addNewProduct("Xbox One", "AXB124AXY", 399);
        inventory.addNewProduct("Samsung TV", "S40AZBDE4", 599);

        assertEquals(inventory.htmlOpening(inventory.htmlHead(inventory.htmlTitle()), inventory.htmlBody(inventory.htmlTable(inventory.htmlThread()))),
                "<html>\n<head>\n\t<title>Inventory</title>\n</head>\n" +
                        "<body>\n\t<h1>Product</h1>\n" +
                        "\t<table>\n\t\t<thread>\n" +
                        "\t\t<tr>\n\t\t\t<th>Name</th>" +
                        "\n\t\t\t<th>Serial Number</th>" +
                        "\n\t\t\t<th>Value</th>" +
                        "\n\t\t</tr>" +
                        "\n\t\t</thread>" +
                        "\n\t\t<thread>\n" +
                        "\t\t<tr>\n\t\t\t<th>Xbox One</th>" +
                        "\n\t\t\t<th>AXB124AXY</th>" +
                        "\n\t\t\t<th>$399.00</th>" +
                        "\n\t\t</tr>" +
                        "\n\t\t</thread>" +
                        "\n\t\t<thread>\n" +
                        "\t\t<tr>\n\t\t\t<th>Samsung TV</th>" +
                        "\n\t\t\t<th>S40AZBDE4</th>" +
                        "\n\t\t\t<th>$599.00</th>" +
                        "\n\t\t</tr>" +
                        "\n\t\t</thread>\n\t</table>\n</body>\n</html>");

    }

    @Test
    public void search_find_by_name() {
        inventory.addNewProduct("Xbox One", "AXB124AXY", 399);
        inventory.addNewProduct("Samsung TV", "S40AZBDE4", 599);

        assertEquals(inventory.searchItem("Samsung TV"), ("Found product Samsung TV, S40AZBDE4, $599.00"));
    }

    @Test
    public void search_find_by_serial() {
        inventory.addNewProduct("Xbox One", "AXB124AXY", 399);
        inventory.addNewProduct("Samsung TV", "S40AZBDE4", 599);

        assertEquals(inventory.searchItem("AXB124AXY"), ("Found product Xbox One, AXB124AXY, $399.00"));
    }

    @Test
    public void search_find_by_double() {
        inventory.addNewProduct("Xbox One", "AXB124AXY", 399);
        inventory.addNewProduct("Samsung TV", "S40AZBDE4", 599);

        assertEquals(inventory.searchItem(599.0), ("Found product Samsung TV, S40AZBDE4, $599.00"));
    }

    @Test
    public void search_not_find_by_double() {
        inventory.addNewProduct("Xbox One", "AXB124AXY", 399);
        inventory.addNewProduct("Samsung TV", "S40AZBDE4", 599);

        assertEquals(inventory.searchItem(200.50), ("No match found."));
    }


}
